// import './scripts/faLoader';
import $ from 'jquery'
import './styles/app.scss';

// Set Routes to load JS on specific body classes
import Router from './scripts/util/Router';
// import common from './scripts/routes/Common';
import single from './scripts/routes/Single';
import page from './scripts/routes/PageTemplateDefault';
import searchResults from './scripts/routes/searchResults';

// Populate Router instance with DOM routes
const routes = new Router({
  // All pages
  // common,
  single,
  page,
  searchResults,
});

// Load Events
$(document).ready(() => routes.loadEvents());
//document.addEventListener("DOMContentLoaded", () => routes.loadEvents());
