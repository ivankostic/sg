import $ from 'jquery';

export default {
  init() {
    let el = $('#right-sidebar');
    el.addClass("sticky-inside");

    $('.sticky-inside').prepend('<div class="sticky-inside-pad"></div>');
    $(window).scroll(function() {
      if ($('.sticky-inside-pad').length == 0 || $('#main').length == 0) {
        return;
      }

      // window info
      var w_top = $(window).scrollTop();
      var w_hei = $(window).height();
      var w_bot = w_top + w_hei;

      $('.sticky-inside-pad').each(function() {
        var s_par = $(this).parent();
        if (!s_par.is('.sticky-inside')) {
          $(this).stop().css('height', 0);
          return;
        }
        var s_ppa = s_par.parent();
        var s_par_top = s_par.offset().top;
        var s_con = $('#main');
        if (s_par.is('.column')) {
          s_con = 0; // reset

          // finding which will be the model for stick to
          s_ppa.find('.column.no-sticky').each(function() {
            if (s_par_top == $(this).offset().top) {
              // we will find all column on same level and has a same top
              if (s_con == 0 || s_con.height() < $(this).height()) {
                // also find the max-height one
                s_con = $(this);
              }
            }
          });
        }

        if (s_con == 0 || s_con.length == 0) {
          $(this).stop().css('height', 0);
          return;
        }

        // content info
        var c_top = s_con.offset().top;
        var c_hei = s_con.height();
        var c_bot = c_top + c_hei;


        // sidebar information
        var s_pad = $(this).height(); // current paddder hei
        var s_top = $(this).offset().top + s_pad;
        var s_hei = s_par.height() - s_pad;
        var s_bot = s_top + s_hei;


        if (s_hei >= c_hei || s_ppa.width() <= s_par.width()) {
          // don't need stick if side hei <= content hei
          $(this).stop().css('height', 0);
          return;
        }

        if (s_hei + s_pad >= c_hei && w_top > s_top) {
          // prevent bounce of first load
          // when height is still not stable
          // because image or iframe loading
          $(this).stop().css('height', (c_hei - s_hei) + 'px');
          return;
        }

        // couting new height of padder
        var new_pad = s_pad;
        if (w_top <= c_top) {
          // scrolled over top of content
          new_pad = 0;
        } else if (w_bot >= c_bot) {
          // scrolled over bot of content
          new_pad = s_pad + c_bot - s_bot;
        } else if (w_top < s_top) {
          // scrolled over top of sidebar
          new_pad = w_top - c_top;
        } else if (w_bot > s_bot) {
          // scrolled over bot of sidebar
          new_pad = s_pad + w_bot - s_bot - 30; /*30px is padding from bottom*/
        }

        // never scroll out of content hei
        if (new_pad + s_hei > c_hei) {
          new_pad = c_hei - s_hei;
        }

        // animate the height
        if (new_pad != s_pad) {
          $(this).stop().animate({
            height: new_pad + 'px',
          }, Number(100));
        }
      });
    });
  },
}
