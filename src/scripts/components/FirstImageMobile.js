import $ from 'jquery';

export default {
  init() {
    //$(window).on('resize', function () {
      if ( this.detectFirstImage() ) {
        this.moveParagraphUp();
      }
    //});
  },
  detectFirstImage() {
    let images = [
      '.entry-content > p > img',
      '.entry-content > img',
      '.entry-content > p > figure',
      '.entry-content > figure',
      '.entry-content > a > img',
    ]

    let imageFound = images.filter( image => $(image).length > 0 );
    // console.log(document.querySelector(imageFound));
    console.log('First image detected!');
    return imageFound ? true : false
  },
  moveParagraphUp() {
    $(window).on('load resize', function () {
      let paragraph = $('.entry-content p:nth-of-type(2)').css('background', 'red');

      if( $(document).width() < 768 ) {
        console.log('mobilee');

        $('.entry-content').prepend(paragraph);
        //return true;
      } else {
        console.log('desktopp');
        $('.entry-content p:nth-of-type(1)').prepend(paragraph);
        //return false;
      }
    })

  },
}
