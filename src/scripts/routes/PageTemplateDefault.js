// JS components
import StickyWidget from '../components/StickyWidget'

export default {
  init() {
    // scripts here run on the DOM load event
    StickyWidget.init();
    console.log('This is Page JS.');
  },
  finalize() {
    // scripts here fire after init() runs
  },
};
