<?php
/**
 * Component Name: Posts
 *
 * Component for displaying posts section
 *
 * @package imwp
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$title  = get_sub_field( 'posts_title' );
$title_color  = get_sub_field( 'title_color' );
$is_title_linkable = get_sub_field( 'is_title_linkable' );
$cat_id = get_sub_field( 'cat_id' );
$background_color = get_sub_field( 'background_color' );
$text_color = get_sub_field( 'text_color' );
$btn_color_scheme = get_sub_field( 'btn_color_scheme' ) ? get_sub_field( 'btn_color_scheme' ) : 'primary';
$content = get_sub_field( 'content' );
$content_text_color =  get_sub_field( 'content_text_color' ) ? get_sub_field( 'content_text_color' ) : '#333';
$display_category_button = get_sub_field( 'display_category_button' );

$args = array(
    'post_type' => 'post',
    'cat' => $cat_id,
    'posts_per_page' => 6,
);

$query = new WP_Query( $args );
?>
<section
  class="fc fc--posts posts section-bg--primary"
  style="background-color: <?= $background_color ?>"
  >
  <div class="grid-container">
    <h2
      class="posts__title"
      style="color: <?= $title_color ? $title_color : '#333' ?>">
      <?php if ( $is_title_linkable ) :
        $custom_title_link = get_sub_field( 'custom_title_link' );
      ?>
        <a
          style="color: <?= $title_color ? $title_color : '#333' ?>"
          href="<?= $custom_title_link ? $custom_title_link : get_permalink( get_option( 'page_for_posts' ) ); ?>">
      <?php endif; ?>

            <?= $title ? $title : ''; ?>

      <?php if ( $is_title_linkable ) : ?>
        </a>
      <?php endif; ?>
    </h2>
    <?php if ( $content ) : ?>
      <div
        class="posts__content"
        style="color: <?= $content_text_color ?>">
        <?= $content; ?>
      </div>
    <?php endif; ?>
  </div>
  <div class="grid-container grid-container--posts">
    <?php
    if ( $query->have_posts() ) :
      while ( $query->have_posts() ) : $query->the_post();
    ?>
      <article class="post-item post-item--<?= $text_color ?>">
        <div class="flex-group">
          <a class="post-item__thumb-link" href="<?php the_permalink() ?>">
            <div class="post-item__thumb-container">
              <?php
              $image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), "medium" );
              $image_height = $image[2];
              ?>
                <img
                class="post-item__thumb <?php echo $image_height < 300 ? 'fit-height' : '' ?>"
                src="<?= $image[0] ?>"
                alt="<?= get_the_title() ?>" />
            </div>
          </a>
          <div class="post-item__content">
            <a class="post-item__title-link" href="<?php the_permalink() ?>">
              <h3 class="post-item__title"><?php the_title(); ?></h3>
            </a>

          </div>
          <a
            class="post-item__read-more"
            href="<?php the_permalink() ?>"
          >Read More &#187;</a>
        </div><!-- /flex-group -->
      </article><!-- /post-item -->
    <?php
      endwhile;
    endif;
    wp_reset_query();
    ?>
    <?php if ( $display_category_button ) :
      $custom_button_link = get_sub_field( 'custom_button_link' );
    ?>
    <div class="posts__button">
      <a
        class="btn btn--<?= $btn_color_scheme; ?>"
        href="<?= $custom_button_link ? $custom_button_link : get_permalink( get_option( 'page_for_posts' ) ); ?>">
        More Articles
      </a>
    </div>
    <?php endif; ?>
  </div><!-- /grid-container  -->
</section><!-- /posts -->
<div class="posts__bottom-shape hide-on-mobile"><svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;"><path d="M-1.41,88.31 C191.58,-18.25 293.73,93.25 500.27,96.20 L500.00,0.00 L0.00,0.00 Z" style="stroke: none; fill: <?= $background_color ?>;"></path></svg></div>
<div class="posts__bottom-shape--mobile" style="background-color: <?= $background_color ?>"></div>
