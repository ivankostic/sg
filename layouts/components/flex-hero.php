<?php
/**
 * Component Name: Hero
 *
 * Component for displaying a hero content.
 *
 * @package imwp
 *   <?= $hero_bg ? "style='background-image: url(" . $hero_bg[sizes][large] . ")'" : ""?>
 *   <img src="<?php echo get_image_src($hero_bg[ID], 'large') ?>" alt= "<?php ?>" />
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$hero_bg  = get_sub_field('hero_background');
$hero_title = get_sub_field('hero_title');
?>
<section class="fc fc--hero hero section-bg--primary">
  <div class="hero__mask"></div>
  <div class="grid-container grid-container--flex-md">
    <div class="hero__graphic-container">
      <?php if ( $hero_bg ): ?>
        <img class="hero__graphic" src="<?= $hero_bg ?>" alt="<?= get_bloginfo('title'); ?>" width="495" />
      <?php endif; ?>
    </div>
    <div class="hero__container">
      <div>
      <?php if( $hero_title ) : ?>
        <h2 class="hero__title"><?= $hero_title; ?></h2>
      <?php endif; ?>
      <?php echo get_sub_field('hero_content'); ?>
      </div>
    </div>
  </div>
  <div class="hero__bottom-shape"></div>
</section><!-- /hero -->
