<?php
/**
 * Component Name: Logos
 *
 * Component for displaying logos section
 *
 * @package imwp
 *   <?= $hero_bg ? "style='background-image: url(" . $hero_bg[sizes][large] . ")'" : ""?>
 *   <img src="<?php echo get_image_src($hero_bg[ID], 'large') ?>" alt= "<?php ?>" />
 */

if ( ! defined( 'ABSPATH' ) ) {
  exit; // Exit if accessed directly.
}

$title = get_sub_field('logos_title');
$use_one_image = get_sub_field('use_one_image');
//print_r($has_one_image);
?>
<section class="fc fc--logos logos section-bg--primary">
  <div class="grid-container">
    <h2 class="logos__title"><?= $title ? $title : ''; ?></h2>
    <div class="logos__container">
    <?php
    if( !$use_one_image ) :
      // check if the repeater field has rows of data
      if( have_rows('logos') ):
       	// loop through the rows of data
          while ( have_rows('logos') ) : the_row();

              // display a sub field value
              $logo = get_sub_field('logo');
              $link = get_sub_field('link') ? get_sub_field('link') : '#';
              if ( $logo ) :
      ?>
              <div class="logos__logo">
                <img class="logos__image" src="<?= $logo['url']; ?>" alt="<?= $logo['alt'] ?>" />
              </div>
      <?php
              endif;
          endwhile;
      endif;
    else:
      $desktop_image = get_sub_field('logos_desktop');
      $mobile_image = get_sub_field('logos_mobile');
    ?>
    <picture class="logos__image">
      <source srcset="<?= $desktop_image['url'] ?>"
              media="(min-width: 768px)">
      <img src="<?= $mobile_image['url'] ?>"  alt="<?= $title ? $title : ''; ?>"/>
    </picture>
    <?php
      endif;
    ?>
    </div><!-- /logos__container -->
  </div><!-- /grid-container  -->
</section><!-- /logos -->
