<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Import ACF fields from earlier exported PHP.
 */
require get_stylesheet_directory() . '/inc/acf-sync.php';

/**
 * Parent theme customizations via hooks
 */
require get_stylesheet_directory() . '/inc/hooks.php';

/**
 * Overriden functions from parent theme
 */
require get_stylesheet_directory() . '/inc/parent-theme-functions.php';

/**
 * Custom widgets
 */
require get_stylesheet_directory() . '/inc/widgets/class-awd-footer-info.php';
require get_stylesheet_directory() . '/inc/widgets/class-awd-pick-posts.php';

/**
 * Shortcodes
 */
require get_stylesheet_directory() . '/inc/shortcodes.php';


/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'imwp_scripts', 0 );
if ( ! function_exists( 'imwp_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function imwp_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version;
		wp_enqueue_style( 'generate-style-grid', get_template_directory_uri() . "/css/unsemantic-grid{$lite}{$suffix}.css", false, GENERATE_VERSION, 'all' );
		wp_enqueue_style( 'generate-style', get_template_directory_uri() . "/style{$suffix}.css", array(), null, 'all' );
		wp_enqueue_style( 'generate-mobile-style', get_template_directory_uri() . "/css/mobile{$suffix}.css", array( 'generate-style' ), GENERATE_VERSION, 'all' );
		wp_enqueue_style( 'sg', get_stylesheet_directory_uri() . '/dist/styles/app.bundle.css', array(), $css_version );

		$js_version = $theme_version;
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'sg', get_stylesheet_directory_uri() . '/dist/scripts/app.bundle.js', array(), $js_version, true );
		// wp_enqueue_script( 'fa-solid', get_stylesheet_directory_uri() . '/dist/scripts/fas.bundle.js', array(), $js_version, true );
		// wp_enqueue_script( 'fa-brand', get_stylesheet_directory_uri() . '/dist/scripts/fab.bundle.js', array(), $js_version, true );
	}
} // endif

/**
 * Dequeue default parent theme styles and scripts
 *
 */
function wp_unload() {
   wp_dequeue_style( 'generate-child' );
	 wp_dequeue_style( 'font-awesome' );
	 wp_dequeue_style( 'essb-fontawsome' );
   wp_dequeue_script( 'generate-a11y' );
	 wp_dequeue_script( 'generate-style' );
	 wp_dequeue_script( 'generate-style-grid' );
	 wp_dequeue_script( 'generate-mobile-style' );
	 wp_dequeue_script( 'jquery' );

}
add_action( 'wp_print_styles', 'wp_unload', 999999999 );


/**
 * Enable SVG imports
 *
 */
function add_file_types_to_uploads($file_types){
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );

  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');


/**
 * Responsive Image Helper Function
 *
 * @param string $image_id the id of the image (from ACF or similar)
 * @param string $image_size the size of the thumbnail image or custom image size
 * @param string $max_width the max width this image will be shown to build the sizes attribute
 */
function acf_responsive_image($image_id,$image_size,$max_width){
	// check the image ID is not blank
	if($image_id != '') {
		// set the default src image size
		$image_src = wp_get_attachment_image_url( $image_id, $image_size );
		// set the srcset with various image sizes
		$image_srcset = wp_get_attachment_image_srcset( $image_id, $image_size );
		// generate the markup for the responsive image
		echo 'src="'.$image_src.'" srcset="'.$image_srcset.'" sizes="(max-width: '.$max_width.') 100vw, '.$max_width.'"';
  }
}

if ( !is_admin() ) wp_deregister_script('jquery');


add_action( 'wpseo_og_article_published_time', 'wpseo_og_published_date_null' );
function wpseo_og_published_date_null(){
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
 		return get_the_modified_date('c');
	} else {
		return get_the_date('c');
	}
}

add_filter( 'wpseo_schema_article', 'yoast_modify_schema_graph_pieces' );
add_filter( 'wpseo_schema_webpage', 'yoast_modify_schema_graph_pieces' );
function yoast_modify_schema_graph_pieces( $data ) {
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$data['datePublished'] = get_the_modified_date('c');
	}

	return $data;
}


/**
 * Custom Breadbrubms page link for specific categories
 * (Yoast Breadcrumbs)
 */
add_filter( 'wpseo_breadcrumb_links', 'custom_wpseo_breadcrumb_output' );
function custom_wpseo_breadcrumb_output( $links ){

	$custom_breadcrumbs = array();
	$i = 0;

	// Loop through breadcrumbs items
	foreach ($links as $item) {

		$term_id = $item['term']->term_id;
		$use_custom_breadcrumbs_page = get_field( 'use_custom_breadcrumbs_page', 'term_' . $term_id );

		if ( $use_custom_breadcrumbs_page || $item['id'] === get_the_ID() ) {

			$custom_page_breadcrumbs = get_field( 'custom_page_breadcrumbs', 'term_' . $term_id );
			$custom_page_breadcrumbs_title = get_field( 'custom_page_breadcrumbs_title', $custom_page_breadcrumbs[0] );
			$page_title = $custom_page_breadcrumbs_title ? $custom_page_breadcrumbs_title : get_the_title( $custom_page_breadcrumbs[0] );

			$custom_breadcrumbs[$i] = array(
				'page_id_TESTING' => $custom_page_breadcrumbs[0],
				'page_title' => $page_title,
				'page_url' => get_the_permalink( $custom_page_breadcrumbs[0] )
			);

			$links[$i] = null; // Get rid of default Category array item
			$links[$i]['url'] = $custom_breadcrumbs[$i]['page_url'];
			$links[$i]['text'] = $custom_breadcrumbs[$i]['page_title'];
		}

		$i++;
	}

	return $links;
}
