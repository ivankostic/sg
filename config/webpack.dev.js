const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const config = require('./config.js');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
//const PurifyCSSPlugin = require('purifycss-webpack');
// const path = require('path');
// const glob = require('glob-all');

module.exports = merge(common, {
  mode: 'development',
  devtool: '#source-map',
  module: {
    rules:  [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            { loader: 'css-loader', options: { sourceMap: true , minimize: false } },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true,
                plugins: [
                  require('autoprefixer')(),
                ],
              },
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true
              }
            },
          ],
        }),
      },
    ],

  },
  devServer: {
    historyApiFallback: true,
    compress: true,
    https: config.url.indexOf('https') > -1 ? true : false,
    publicPath: config.fullPath,
    proxy: {
          '*': {
              'target': config.url,
              'secure': false
          },
          '/': {
              target: config.url,
              secure: false
          }
      },
  },
  plugins: [
    new ExtractTextPlugin({
        filename:  (getPath) => {
            return getPath('../styles/[name].bundle.css').replace('css/js', 'css');
        },
        allChunks: true
    }),
    new StyleLintPlugin({
      failOnError: false,
      syntax: 'scss',
    }),
    new BrowserSyncPlugin( {
        proxy: config.url,
        files: [
            '**/*.php'
        ],
        reloadDelay: 0
    }
    ),
  ],
})
