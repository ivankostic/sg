const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const PurifyCSSPlugin = require('purifycss-webpack');
const path = require('path');
const glob = require('glob-all');
//const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  devtool: 'false',
  // optimization: {
  //   minimizer: [
  //     new UglifyJsPlugin({
  //       extractComments: true
  //     })
  //   ]
  // },
  module: {
     rules: [
       {
         test: /\.scss$/,
          use: ExtractTextPlugin.extract({
           fallback: 'style-loader',
           use: [
             { loader: 'css-loader', options: { sourceMap: false, minimize: true  } },
             {
               loader: 'postcss-loader',
               options: {
                 sourceMap: false,
                 plugins: [
                   require('autoprefixer')(),
                   require('cssnano')({ preset: 'default' }),
                 ],
               },
             },
             { loader: 'sass-loader', options: { sourceMap: false } },
           ],
         }),
       },
     ],

   },
   plugins: [
     new ExtractTextPlugin({
         filename:  (getPath) => {
           return getPath('../styles/[name].bundle.css').replace('css/js', 'css');
         },
         allChunks: true
     }),
     //  new PurifyCSSPlugin({
     //    paths: glob.sync([
     //      path.join(__dirname, '../*.php'),
     //      path.join(__dirname, '../templates/*.php'),
     //      path.join(__dirname, '../woocomerce/*.php'),
     //      path.join(__dirname, '../inc/*.php'),
     //      path.join(__dirname, '../inc/vendors/*.php'),
     //      path.join(__dirname, '../layouts/components/*.php'),
     //      path.join(__dirname, '../layouts/components/loops/*.php'),
     //      path.join(__dirname, '../src/*.js'),
     //      path.join(__dirname, '../src/scripts/*.js'),
     //      path.join(__dirname, '../src/scripts/components/*.js'),
     //      path.join(__dirname, '../src/scripts/routes/*.js'),
     //      path.join(__dirname, '../src/scripts/vendors/*.js'),
     //      path.join(__dirname, '../src/scripts/util/*.js'),
     //    ]),
     //    minimize: true,
     //    purifyOptions: {
     //      whitelist: [
     //        '*alignright*',
     //        '*alignleft*',
     //        '*rounded-circle*',
     //        '.profile-name',
     //        '.doug',
     //        '.paul',
     //        '.nathan',
     //        '.colleen',
     //        '.progress',
     //        '.progress-bar',
     //        '.progress-bar-striped',
     //        '.progress-bar-animated',
     //        'blockquote',
     //        '.client-name',
     //        '.hero__subtitle',
     //        '.entry-featured-image',
     //        '.csbwfs-social-widget',
     //        '#csbwfs-social-inner',
     //        '.csbwfs-sbutton',
     //        '.csbwfs-fb',
     //        '.csbwfs-tw',
     //        '.csbwfs-gp',
     //        '.csbwfs-li',
     //        '.csbwfs-pin',
     //        '.csbwfs-re',
     //        '.show-nav'
     //      ]
     //    }
     // }),
  ],
})
