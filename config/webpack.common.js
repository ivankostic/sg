const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
//const webpack = require('webpack');

module.exports = {
  entry: {
    app: './src/app.js',
    // fas: './src/scripts/fasLoader.js',
    // fab: './src/scripts/fabLoader.js',
  },
  output: {
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, '../dist/scripts/'),
  },
  resolve: {
    extensions: ['.js'],
  },
  module: {
    rules : [
      {
        test: /\.(js)$/,
        exclude: /(node_modules)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  "@babel/preset-env",
                  {
                    "useBuiltIns": "entry",
                  },
                ],
              ],
            },
          }, {
            loader: 'eslint-loader',
          },
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: {
          loader: "url-loader",
          options: {
            limit: 25000,
            name: "[hash].[ext]",
            outputPath: '../images/',
          },
        },
      },
      {
         test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: {
           loader: 'file-loader',
           options: {
              name: '[name].[ext]',
              outputPath: '../fonts/',
            },
         },
      },
      {
        test: /bootstrap\/dist\/js\/umd\//, use: 'imports-loader?jQuery=jquery',
      },
    ],
  },
  plugins: [
    // new webpack.ProvidePlugin({
    //   $: 'jquery',
    //   jQuery: 'jquery',
    //   'window.jQuery': 'jquery',
    // }),
    new CleanWebpackPlugin(['dist']),
  ],
  externals: {
    jquery: 'jQuery',
  },
}
