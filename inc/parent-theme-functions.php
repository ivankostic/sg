<?php

if ( ! function_exists( 'generate_posted_on' ) ) :
/**
* Prints HTML with meta information for the current post-date/time and author.
*/
function generate_posted_on()
{
	$date = apply_filters( 'generate_post_date', true );
	$author = apply_filters( 'generate_post_author', true );

  $url = get_author_posts_url( get_the_author_meta( 'ID' ) );
  $custom_avatar = get_field( 'user_avatar', 'user_' . get_the_author_meta( 'ID' ) );
  $gravatar = get_author_posts_url( get_the_author_meta( 'ID' ) );

  $image = null;

  // If custom avatar isn't uploaded, try to grab from Gravatar
  if ( $custom_avatar ) {
    $image = $custom_avatar;
  } elseif ( !$custom_avatar && $gravatar ) {
    $image = get_avatar_url( get_the_author_meta( 'ID' ), ['size' => '28'] );
  }

  if ( $image ) {
    $image_string ='<img class="vcard__image"' .
      'src="'. $image .'"' .
      'alt="'. $name .'" width="28" height="28"/>';
  }

	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="updated" datetime="%3$s" itemprop="dateModified">%4$s</time>';
  } else {
		$time_string = '<time class="entry-date published" datetime="%1$s" itemprop="datePublished">%2$s</time>';
	}


  // get_option( 'date_format' )
	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( get_option( 'date_format' ) ) ),
		esc_html( get_the_date( 'j F, Y' ) ),
		esc_attr( get_the_modified_date( get_option( 'date_format' ) ) ),
		esc_html( get_the_modified_date( 'j F, Y' ) )
	);

 // If our author is enabled, show it
	if ( $author ) :
		printf( ' <div class="byline">%1$s</div>',
			sprintf( '<div class="author vcard" itemtype="http://schema.org/Person" itemscope="itemscope" itemprop="author">%1$s <a class="url fn n" href="%2$s" title="%3$s" rel="author" itemprop="url">'. $image_string .'<span class="author-name" itemprop="name">%4$s</span></a></div>',
				__( '','generatepress'),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( __( 'View all posts by %s', 'generatepress' ), get_the_author() ) ),
				esc_html( get_the_author() )
			)
		);
	endif;

	// If our date is enabled, show it
	if ( $date ) :
		printf( ' <div class="posted-on">%1$s</div>',
			sprintf( '<a href="%1$s" title="%2$s" rel="bookmark">%3$s</a>',
				esc_url( get_permalink() ),
				esc_attr( get_the_time() ),
				$time_string
			)
		);
	endif;

}
endif;

/**
* Remove Google fonts preconnect
*/
add_action( 'after_setup_theme', 'tu_remove_google_preconnect' );
function tu_remove_google_preconnect() {
    remove_filter( 'wp_resource_hints', 'generate_resource_hints', 10, 2 );
}
