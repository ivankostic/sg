<?php
/**
 *  Pick Posts Widget
 *
 * @package AWD
 * @author Ivan Kostic
 * @link http://www.awebdeveloper.co.uk
 * @since SG Theme 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class AWD_PickPosts extends WP_Widget {

  /**
  * Sets up the widgets name etc
  */
  public function __construct() {
    $widget_ops = array(
      'classname' => 'pick_post_widget',
      'description' => 'Pick 5 posts to display.',
    );
    parent::__construct( 'AWD_PickPosts', 'Pick Posts Widget', $widget_ops );
  }

  /**
  * Outputs the content of the widget
  *
  * @param array $args
  * @param array $instance
  */
  public function widget( $args, $instance ) {
    // outputs the content of the widget
		// outputs the content of the widget
      if ( ! isset( $args['widget_id'] ) ) {
        $args['widget_id'] = $this->id;
      }

      // widget ID with prefix for use in ACF API functions
      $widget_id = 'widget_' . $args['widget_id'];

      $title = get_field( 'title', $widget_id ) ? get_field( 'title', $widget_id ) : '';
      $pick_posts = get_field( 'pick_posts', $widget_id );

      echo $args['before_widget'];

      if ( $title ) {
        echo $args['before_title'] . esc_html($title) . $args['after_title'];
      }

			$ids = array_values( $pick_posts );

			$args = array(
			   'post__in' => $ids,
				 'orderby' => 'post__in'
			);

			$query = new WP_Query( $args );


			echo '<ul>';

			while ( $query->have_posts() ) {
			    $query->the_post();
			    echo '<li>';
					echo '<a href="' . get_the_permalink() . '">' . get_the_title() . '</a>';
					echo '<span class="post-date">' . get_the_date() . '</span>';
					echo '</li>';
			}

			echo '</ul>';


      echo $args['after_widget'];

			wp_reset_query();
  }

  /**
   * Outputs the options form on admin
   *
   * @param array $instance The widget options
   */
  public function form( $instance ) {
  	// outputs the options form on admin
  }

  /**
   * Processing widget options on save
   *
   * @param array $new_instance The new options
   * @param array $old_instance The previous options
   *
   * @return array
   */
  public function update( $new_instance, $old_instance ) {
  	// processes widget options to be saved
  }

}

// Register and load the widget
function wp_load_widget_pick_posts() {
    register_widget( 'AWD_PickPosts' );
}
add_action( 'widgets_init', 'wp_load_widget_pick_posts' );
