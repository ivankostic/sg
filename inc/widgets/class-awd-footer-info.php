<?php
/**
 *  Footer Info Widget
 *
 * @package AWD
 * @author Ivan Kostic
 * @link http://www.awebdeveloper.co.uk
 * @since SG Theme 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Register and load the widget
function wp_load_widget() {
    register_widget( 'AWD_FooterInfo' );
}
add_action( 'widgets_init', 'wp_load_widget' );

// Enqueue additional admin scripts
add_action('admin_enqueue_scripts', 'load_scripts');
function load_scripts() {
    wp_enqueue_media();
    wp_enqueue_script('upload-image', get_stylesheet_directory_uri() . '/inc/widgets/upload-image.js', false, '1.0.0', true);
}

// Widget class
class AWD_FooterInfo extends WP_Widget {

	function __construct() {
		$widget_ops = array( 'classname' => 'widget-footer-info', 'description' => __( 'Display info in site footer.' ) );
		parent::__construct( 'awd-footer-info', __( 'Footer info' ), $widget_ops );
	}

	function widget( $args, $instance ) {

		$title = apply_filters( 'widget_title', $instance[ 'title' ] );
		if ( $title ) {
			echo $args['before_widget'] . $args['before_title'] . $title . $args['after_title'];
		} else {
			echo $args['before_widget'];
		}

		$logo_url = $instance[ 'logo_url' ];
		$fb = $instance[ 'fb' ];
		$tw = $instance[ 'tw' ];
		$in = $instance[ 'in' ];
		$yt = $instance[ 'yt' ];
		$sc = $instance[ 'sc' ];
	?>
			<div class="footer-info"
					 style="text-align: center"
			>
				<img src="<?php echo esc_url($logo_url); ?>" alt="<?php echo get_bloginfo('title'); ?>" />
				<p class="copy">&copy; Copyright <?php echo date('Y') ?> - All rights reserved</p>
				<ul class="social-icons">
				<?php if ( $fb ) : ?>
					<li>
						<a target="_blank" rel="noopener noreferrer" class="social-icon social-icon--fb" href="<?php echo $fb; ?>"><i class="fab fa-facebook-f"></i></a>
					</li>
				<?php endif; ?>
				<?php if ( $tw ) : ?>
					<li>
						<a target="_blank" rel="noopener noreferrer" class="social-icon social-icon--tw" href="<?php echo $tw; ?>"><i class="fab fa-twitter"></i></a>
					</li>
				<?php endif; ?>
				<?php if ( $in ) : ?>
					<li>
						<a target="_blank" rel="noopener noreferrer" class="social-icon social-icon--in" href="<?php echo $in; ?>"><i class="fab fa-instagram"></i></a>
					</li>
				<?php endif; ?>
				<?php if ( $yt ) : ?>
					<li>
						<a target="_blank" rel="noopener noreferrer" class="social-icon social-icon--yt" href="<?php echo $yt; ?>"><i class="fab fa-youtube"></i></a>
					</li>
				<?php endif; ?>
				<?php if ( $sc ) : ?>
					<li>
						<a target="_blank" rel="noopener noreferrer" class="social-icon social-icon--sc" href="<?php echo $sc; ?>"><i class="fab fa-snapchat-ghost"></i></a>
					</li>
				<?php endif; ?>
				</ul>
			</div>
		<?php
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = strip_tags( $new_instance['title'] );
	  $instance['logo_url'] = strip_tags( $new_instance['logo_url'] );
	  $instance['fb']  			= strip_tags( $new_instance['fb'] );
	  $instance['tw']  		  = strip_tags( $new_instance['tw'] );
	  $instance['in']       = strip_tags( $new_instance['in'] );
	  $instance['yt']       = strip_tags( $new_instance['yt'] );
	  $instance['sc']       = strip_tags( $new_instance['sc'] );

		return $instance;
	}

	function form( $instance ) {
		$instance = wp_parse_args( (array) $instance, array(
			'title'  => '',
			'logo_url' => '',
			'fb'   => '',
			'tw'  => '',
			'in'  => '',
			'yt'  => '',
			'sc'  => '',
		));

		$title 		= $instance['title'];
	  $logo_url = $instance['logo_url'];
	  $fb 			= $instance['fb'];
	  $tw 			= $instance['tw'];
	  $in 			= $instance['in'];
	  $yt 		  = $instance['yt'];
	  $sc 			= $instance['sc'];
		?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">
				<?php _e( 'Widget Title:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>"
							 name="<?php echo $this->get_field_name( 'title' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $title ); ?>"/>
		</p>
		<p>
		<label for="<?= $this->get_field_id( 'logo_url' ); ?>">
			<?php _e( 'Logo:' ); ?></label>
			<img class="<?= $this->id ?>_img" src="<?= (!empty($instance['logo_url'])) ? $instance['logo_url'] : ''; ?>"/>
			<input type="text" class="widefat <?= $this->id ?>_url" name="<?= $this->get_field_name( 'logo_url' ); ?>" value="<?= $instance['logo_url']; ?>" style="margin-top:5px;" />
 			<input type="button" id="<?= $this->id ?>" class="button button-primary js_custom_upload_media" value="Upload Image" style="margin-top:5px;" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'fb' ); ?>">
				<?php _e( 'Facebook:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'fb' ); ?>"
							 name="<?php echo $this->get_field_name( 'fb' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $fb ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tw' ); ?>">
				<?php _e( 'Twitter:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'tw' ); ?>"
							 name="<?php echo $this->get_field_name( 'tw' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $tw ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'in' ); ?>">
				<?php _e( 'Instagram:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'in' ); ?>"
							 name="<?php echo $this->get_field_name( 'in' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $in ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'yt' ); ?>">
				<?php _e( 'YouTube:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'yt' ); ?>"
							 name="<?php echo $this->get_field_name( 'yt' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $yt ); ?>"/>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'sc' ); ?>">
				<?php _e( 'Snapchat:' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'sc' ); ?>"
							 name="<?php echo $this->get_field_name( 'sc' ); ?>"
							 type="text"
							 value="<?php echo esc_attr( $sc ); ?>"/>
		</p>
		<?php
	}
}
// register_widget( 'AWD_Footer_info' );
