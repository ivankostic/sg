<?php
/**
 * Wp hooks/filters from parent theme
 *
 * @package imwp
 */

 if ( ! defined( 'ABSPATH' ) ) {
 	exit;
 }

 /**
  * Custom Author box
  */
 add_filter( 'generate_after_entry_content' ,'add_author_box', 1 );
 function add_author_box() {
   if ( is_single() ) {
     get_template_part('templates/authorbox');
   }
 }

 /**
  * Pagination on archive pages
  */
//  if ( ! function_exists( 'generate_paging_nav' ) ) {
//  	function generate_paging_nav() {
//  		if ( function_exists( 'the_posts_pagination' ) ) {
//  			the_posts_pagination( array(
//  				'mid_size' => apply_filters( 'generate_pagination_mid_size', 1 ),
//  				'prev_text' => __( '<i class="fas fa-angle-left"></i>', 'generatepress' ),
//  				'next_text' => __( '<i class="fas fa-angle-right"></i>', 'generatepress' )
//  			) );
//  		}
//  	}
//  }
// // Add pagination above
// add_filter( 'generate_archive_title' ,'add_pagination_above', 50 );
//  function add_pagination_above() {
//    generate_content_nav( 'nav-above' );
// }


/**
 * Custmoized archive title to include "Category"
 */
// add_filter( 'get_the_archive_title' ,'custom_archive_title' );
// function custom_archive_title($title) {
//   if ( is_category() ) {
//     $title = '<span>Category:</span> ';
//   } else {
//     $title = '';
//   }
//   echo $title;
// }


/**
 * Change theme's read more button
 */
// add_filter( 'generate_excerpt_more_output','lh_change_read_more' );
// function lh_change_read_more() {
//     return sprintf( ' [...] <a title="%1$s" class="read-more" href="%2$s">Read more</a>',
//         the_title_attribute( 'echo=0' ),
//         esc_url( get_permalink( get_the_ID() ) )
//     );
// }


/**
 * Enable form validation for comments
 */
add_theme_support( 'html5', array( 'comment-form' ) );
function custom_enable_comment_form_validation() {
	if ( is_single() && comments_open() && current_theme_supports( 'html5' ) ) {
		echo '<script>document.getElementById("commentform").removeAttribute("novalidate");</script>' . PHP_EOL;
	}
}
add_action( 'wp_footer', 'custom_enable_comment_form_validation' );


/**
 * Enable Yoast breadcrumbs
 */
add_action( 'generate_before_comments_container', 'add_yoast_breadcrumbs');
add_action( 'generate_inside_container', 'add_yoast_breadcrumbs');
function add_yoast_breadcrumbs() {
  if ( !is_search() && !is_home() && !is_front_page() ) :
    if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<div class="grid-container breadcrumbs-container"><div class="breadcrumbs">','</div></div>' );
    }
  endif;
};


/**
 * Modify fields for comments
 */
add_filter( 'comment_form_default_fields', 'tu_filter_comment_fields', 20 );
function tu_filter_comment_fields( $fields ) {
    $commenter = wp_get_current_commenter();

    $fields['author'] = '<p class="comment-field comment-field--author"><label for="author" class="">' . esc_html__( 'Name', 'generatepress' ) . '</label><input required aria-required id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" /></p>';
    $fields['email'] = '<p class="comment-field comment-field--email"><label for="email" class="">' . esc_html__( 'Email', 'generatepress' ) . '</label><input required aria-required id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" /></p>';
    $consent   = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

    unset( $fields['url'] );

    return $fields;
}


/**
 * Remove top footer bar from parent theme
 */
add_action( 'after_setup_theme','tu_remove_footer' );
function tu_remove_footer() {
  remove_action( 'generate_footer','generate_construct_footer' );
}


/**
 * Add wave shape in footer
 */
add_action( 'generate_before_footer_content','add_footer_wave' );
function add_footer_wave() {
  echo '<div class="wave wave--footer" style="overflow: hidden;" ><svg viewBox="0 0 500 150" preserveAspectRatio="none" style="height: 100%; width: 100%;"><path d="M-3.38,151.47 C269.19,-67.59 194.13,290.63 516.36,70.55 L500.00,150.00 L25.96,259.03 Z" style="stroke: none; fill: #363636;"></path></svg></div>';
}
