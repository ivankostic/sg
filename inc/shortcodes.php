<?php
/**
 * Shortcodes used in the theme
 *
 * @package imwp
 */

 /* ------------------------------------------------------------------------ */
 /*  Leadtext shortcode
 /*  [leadtext border="red/orange/blue/green/#565656/whateverColorYouWant"]Lorem Ipsum dolor[/leadtext]
 /* ------------------------------------------------------------------------ */
 function leadtext( $atts, $content = "Lorem Ipsum dolor" ) {

   $a = shortcode_atts( array(
     'border' => 'red',
   ), $atts );

   $html = '<div class="leadtext" style="border-color:' . $a['border'] . '">' .
        $content .
        '</div>';

  return $html;
}
add_shortcode( 'leadtext', 'leadtext' );


/* ------------------------------------------------------------------------ */
/*  Notebox shortcode
/*  [notebox title="Important!"]Lorem Ipsum dolor[/notebox]
/* ------------------------------------------------------------------------ */
function notebox( $atts, $content = "Lorem Ipsum dolor" ) {

  $a = shortcode_atts( array(
    'title' => 'Important!',
  ), $atts );

  $html = '<div class="notebox">' .
            '<h3 class="notebox__title">' . $a['title'] . '</h3>' .
            '<div class="notebox__content">' .
              $content .
            '</div>' .
          '</div>';

 return $html;
}
add_shortcode( 'notebox', 'notebox' );


/* ------------------------------------------------------------------------ */
/*  Callout shortcode
/*  [callout title="Important:" type="important/warning/note"]Lorem Ipsum dolor[/callout]
/* ------------------------------------------------------------------------ */
function callout( $atts, $content = "Lorem Ipsum dolor" ) {

  $a = shortcode_atts( array(
    'title' => 'Important:',
    'type' =>  'important',
  ), $atts );

  $html = '<div class="callout callout--' . $a['type'] . '">' .
            '<p class="callout__title">' .
              '<span class="callout__icon callout__icon--' . $a['type'] . '"></span>' .
              $a['title'] .
            "</p> " .
            $content .
          '</div>';

 return $html;
}
add_shortcode( 'callout', 'callout' );


 /* ------------------------------------------------------------------------ */
 /*  Pros & Cons shortcode
 /*  [proscons title_tag="h3" pros_title="Pros" cons_title="Cons" template=2 pros="Okay lorem // Better lorem ispum" cons="Not Good ispum // The Worst!"]
 /* ------------------------------------------------------------------------ */
 function proscons( $atts ) {

   $a = shortcode_atts( array(
     'pros_title' => 'Pros',
     'cons_title' => 'Cons',
     'pros' => 'Okay lorem ispum // Better lorem ispum // The Best lorem ispum',
     'cons' => 'Not Good lorem ispum // The Worst lorem ispum',
     'template' => 'default',
     'title_tag' => 'h3'
   ), $atts );

   $pros = explode( '//', $a['pros'] );
   $cons = explode( '//', $a['cons'] );

    $html = '<div class="pros-cons-box pros-cons pros-cons-template-' . $a['template'] . '">' .
    '<div class="pros-box">' .
      '<' . $a['title_tag'] .' class="pros-title">' . $a['pros_title'] . '</'. $a['title_tag'] . '>' .
        '<ul>';

          foreach ($pros as $p) {
            $html .= '<li>' . $p . '</li>';
          }

      $html .= '</ul>' .
      '</div>' .
      '<div class="cons-box">' .
        '<' . $a['title_tag'] .' class="cons-title">' . $a['cons_title'] . '</'. $a['title_tag'] . '>' .
        '<ul>';

          foreach ($cons as $c) {
            $html .= '<li>' . $c . '</li>';
          }

        $html .= '</ul>' .
      '</div>' .
    '</div>';

 	return $html;
 }
 add_shortcode( 'proscons', 'proscons' );


/* ------------------------------------------------------------------------ */
/*  IP Scanner shortcode
/*  [ipscanner headline='Title' url='https://google.com' button_text='Click' link_text='A link text']
/*  or just [ipscanner] for default values
/* ------------------------------------------------------------------------ */
function ipscanner( $atts ) {

    $a = shortcode_atts( array(
		'button_text' => 'Change Your IP with NordVPN',
        'link_text' => 'Get a New IP, Visable Location, with a 70% discount!',
        'url' => 'https://go.nordvpn.net/aff_c?aff_id=1758&offer_id=121&url_id=433&aff_sub=SGTool',
        'headline' => 'Your IP and Location are Visible!'
    ), $atts );

   // print_r($a);

  	$scanner_html = "<div id='scanner-container' style='font-size:15px'>" .
    "<h2>Checking Your IP Address...</h2>" .
    "<div id='connection-details'>" .
     "<div id='userdata-container'>".
      "<div class='scanner-rows'>" .
        "<div class='row-info'>" .
          "<p>IP Address</p>" .
        "</div>" .
        "<div class='row-data' elementId='0' header='Checking Your IP Address...'>" .
          "<span style='display:none' class='alert-icon'>!</span>" .
          "<p id='ip-data'></p>" .
        "</div>" .
      "</div>" .
      "<div class='scanner-rows'>" .
        "<div class='row-info'>" .
          "<p>Location</p>" .
        "</div>" .
        "<div class='row-data' elementId='1' header='Checking Your Location...'>" .
          "<span style='display:none' class='alert-icon'>!</span>" .
          "<p id='location-data'></p>" .
        "</div>" .
      "</div>" .
      "<div class='scanner-rows'>" .
        "<div class='row-info'>" .
          "<p>Browser</p>" .
        "</div>" .
        "<div class='row-data' elementId='2' header='Checking Your Browser...'>" .
          "<span style='display:none' class='alert-icon'>!</span>" .
          "<p id='browser-data'></p>" .
        "</div>" .
      "</div>" .
      "<div class='scanner-rows'>" .
        "<div class='row-info'>" .
          "<p>Screen Resolution</p>" .
        "</div>" .
        "<div class='row-data' elementId='3' header='Checking Your Resolution...'>" .
          "<span style='display:none' class='alert-icon'>!</span>" .
          "<p id='screen-data'></p>" .
        "</div>" .
      "</div>" .
       "</div>" .
        "<div id='map-container'></div>" .
    "</div>" .
    "<a id='scanner-cta' target='_blank' href='" . $a['url'] . "' rel='noopener noreferrer'><span></span>" . $a['button_text'] . "</a>" .
    "<a id='scanner-cta-link' target='_blank' href='" . $a['url'] . "' rel='noopener noreferrer'>" . $a['link_text'] . "</a>" .
    "<p id='scanner-headline' style='display:none'>". $a['headline'] . "</p>" .
  "</div>";

	return $scanner_html;
}
add_shortcode( 'ipscanner', 'ipscanner' );

/* ------------------------------------------------------------------------ */
/*  Alternatives shortcode
/*  [alternatives 1="https://securitygladiators.com/wp-content/uploads/2017/12/ipvanish.jpg,9.8,excelent,5.20,#1" 2="https://securitygladiators.com/wp-content/uploads/2017/12/nordvpn.jpg,9.5,excelent,5.75,#2" 3="https://securitygladiators.com/wp-content/uploads/2017/12/privatevpn-1.jpg,9.1,good,5.10,#3"]
/*  or just [alternatives] for default values
/* ------------------------------------------------------------------------ */
function alternatives( $atts ) {

    $a = shortcode_atts( array(
    '1' => "https://securitygladiators.com/wp-content/uploads/2017/12/ipvanish.jpg,9.8,excelent,5.20,https://securitygladiators.com/ipvanish-review/",
		'2' => "https://securitygladiators.com/wp-content/uploads/2017/12/nordvpn.jpg,9.5,excelent,5.75,https://securitygladiators.com/thinking-nordvpn-read-review/",
		'3' => "https://securitygladiators.com/wp-content/uploads/2017/12/privatevpn-1.jpg,9.1,good,5.10,https://securitygladiators.com/privatevpn/",
    ), $atts );

  $html = '';
  $html .= '<div class="alternative-options-widget">';
	$html .= '<h2>Some alternative options for you...</h2>';
	$html .= '<div class="aow-list">';

	foreach ($a as $provider) {

	$provider_attrs = explode(",",$provider) ;
	$html .= '<div class="aow">';
		$html .= '<img src="'. $provider_attrs[0] .'" alt="" />';
		$html .= '<div class="score">';
		$html .= '<p>Our score: <strong>'. $provider_attrs[1] .'</strong></p>';
		$html .= '<span class="score-gear score-'. $provider_attrs[2] .'"></span>';
		$html .= '</div>';
		$html .= '<div class="price">';
		$html .= 'From <strong>$'. $provider_attrs[3] .'</strong> per month';
		$html .= '</div>';
		$html .= '<div class="read-review"><a href="'. $provider_attrs[4] .'">Learn more</a></div>';
	$html .= '</div>';
	}

	$html .= '</div>';
$html .= '</div>';

	return $html;
}
add_shortcode( 'alternatives', 'alternatives' );


/***  'Best for' icons shortcode ***/
function bestfor( $atts ) {

    $a = shortcode_atts( array(
        'icons' => "",
    ), $atts );

    $html = '';
    $icons = explode(",",$a['icons']) ;

    foreach ( $icons as $icon ) {
      //$html .= '<img class="ideal-ico" src="' .  get_stylesheet_directory_uri() . '/img/' . $icon . '.jpg' . '" alt="' . $icon . '" />';
      $html .= '<i class="bg-'.$icon.'" alt="' . $icon . '" ></i>';
    }

	return $html;
}
add_shortcode( 'bestfor', 'bestfor' );
