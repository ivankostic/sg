<?php
// Get specific review table linked to page
$review_table = get_field('review_tables');
$review_table_id = $review_table->ID;
$title = get_the_title($review_table_id);



$sprite['amazon.jpg']['class']='vpn_amazon';
$sprite['bbc.jpg']['class']='vpn_bbc';
$sprite['CyberGhost-VPN-logo.png']['class']='vpn_cyberghostt';
$sprite['expressvpn.png']['class']='vpn_expressvpnt';
$sprite['hbo.jpg']['class']='vpn_hbo';
$sprite['Hide.me-logo.png']['class']='vpn_hidemevpnt';
$sprite['HideMyAss-logo-1.png']['class']='vpn_hmat';
$sprite['hulu.jpg']['class']='vpn_hulu';
$sprite['ipvanish-1.png']['class']='vpn_ipvanisht';
$sprite['kodi.png']['class']='vpn_kodi';
$sprite['netflix.jpg']['class']='vpn_netflix';
$sprite['logotype-horizontal.png']['class']='vpn_nordvpnt';
$sprite['Private-Internet-Access-logo.png']['class']='vpn_piat';
$sprite['PureVPN-logo.png']['class']='vpn_purevpnt';
$sprite['strongvpn-1.png']['class']='vpn_strongvpnt';
$sprite['TorGuard-logo.png']['class']='vpn_torguardt';
$sprite['utorrent.png']['class']='vpn_utorrent';
?>

<div class="review-tables">
  <h1 class="entry-title"><?php echo $title; ?></h1>
<table class="top-table desktop-tbl">
  <thead>
    <tr>
      <th>Provider</th>
      <th>What we like</th>
      <th>More Info</th>
    </tr>
  </thead>
<tbody>
<?php
$i = 0;
	// check if the repeater field has rows of data
	if( have_rows('review_table', $review_table_id) ):

		// loop through the rows of data
		while ( have_rows('review_table', $review_table_id) ) : the_row(); $i++;

	    $name = get_sub_field('name', $review_table_id);
		$logo = get_sub_field('logo', $review_table_id);
	    $sale = get_sub_field('sale', $review_table_id);
	    $desktop_content = get_sub_field('desktop_content', $review_table_id);
	    $score = get_sub_field('score', $review_table_id);
     	$review_link = get_sub_field('review_link', $review_table_id);
	    $d_affiliate_link = get_sub_field('d_affiliate_link', $review_table_id);
?>

<?php if ( empty($sale) ) : ?><tr class="sep"><td colspan="3"><hr></td></tr><?php endif; ?>
<tr <?php if ($sale) : ?>class="featured"<?php endif; ?>>
	<td>
		<?php if ($sale) : ?>
		<div class="ribbon"><span>Editor's choice</span></div>
		<div class="ribbon-save">Save<span><?php echo $sale; ?>%</span></div>
		<?php endif; ?>
		<a class="vpn-logo" href="<?php echo $d_affiliate_link; ?>" target="_blank" rel="nofollow">
		    <?php $file_name = basename($logo) ?>
		    <?php if(isset($sprite[$file_name]) && $sprite[$file_name]){ ?>
		        <i class='<?php echo $sprite[$file_name]["class"]; ?>'></i>
		    <?php }else{ ?>
			<img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>">
			<?php } ?>
		</a>
		<div class="star-ratings">
			<div class="star-ratings-top" style="width:<?php echo $score * 10; ?>%">
				<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
			</div>
			<div class="star-ratings-bottom">
				<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
			</div>
		</div>
		<a class="review-link" href="<?php echo $review_link; ?>">Read <?php echo $name; ?> Review</a>
	</td>
	<td class="pros">
		<?php echo $desktop_content; ?>
	</td>
	<td>
		<p class="score">Our Score<span><?php echo number_format_i18n( $score, 1 ); ?></span></p>
		<a class="visit-link" href="<?php echo $d_affiliate_link; ?>" target="_blank" rel="nofollow">VISIT SITE</a>
	</td>
</tr>
<?php
		endwhile;

	else :
		// no rows found
	endif;
?>
</tbody>
</table><!-- /desktop-tbl -->

<div class="top-table mobile-tbl">
<?php
if( have_rows('review_table', $review_table_id) ) :
	// loop through the rows of data
	while ( have_rows('review_table', $review_table_id) ) : the_row();

	$name = get_sub_field('name', $review_table_id);
	$logo = get_sub_field('logo', $review_table_id);
	$sale = get_sub_field('sale', $review_table_id);
	$mobile_content = get_sub_field('mobile_content', $review_table_id);
	$score = get_sub_field('score', $review_table_id);
	$review_link = get_sub_field('review_link', $review_table_id);
	$m_affiliate_link = get_sub_field('m_affiliate_link', $review_table_id);
?>
  <div class="mobile-tbl-row">
    <div>
		<a class="vpn-logo" href="<?php echo $m_affiliate_link; ?>" target="_blank" rel="nofollow">
		    <?php $file_name = basename($logo) ?>
		    <?php if(isset($sprite[$file_name]) && $sprite[$file_name]){ ?>
		        <i class='<?php echo $sprite[$file_name]["class"]; ?>'></i>
		    <?php }else{ ?>
			    <img src="<?php echo $logo; ?>" alt="<?php echo $name; ?>">
			<?php } ?>
		</a>
		<div class="star-ratings">
			<div class="star-ratings-top" style="width:<?php echo $score * 10; ?>%">
				<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
			</div>
			<div class="star-ratings-bottom">
				<span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
			</div>
		</div>
	  <p class="score">Our Score<span><?php echo number_format_i18n( $score, 1 ); ?></span></p>
	  <a class="review-link" href="<?php echo $review_link; ?>">Read <?php echo $name; ?> Review</a>
    </div>
    <div>
      <?php echo $mobile_content; ?>
      <a class="visit-link" href="<?php echo $m_affiliate_link; ?>" target="_blank" rel="nofollow">VISIT SITE<i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
    </div>
  </div>
<?php
	endwhile;
else :
	// no rows found
endif;
?>
</div><!-- /mobile-tbl -->
</div>
