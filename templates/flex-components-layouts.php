<?php
/**
 * Template Name: Flex Components Layouts
 *
 * Template for displaying created ACF Pro flexible layouts
 *
 * @package imwp
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();

$id = get_the_ID();
?>

<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
	<main id="main" <?php generate_do_element_classes( 'main' ); ?>>
		<div class="<?= is_front_page() ? 'wrapper--overflow' : 'wrapper' ?>">
		<?php
		/**
		 * generate_before_main_content hook.
		 *
		 * @since 0.1
		 */
		do_action( 'generate_before_main_content' );

		while ( have_posts() ) : the_post(); ?>

			<?php
			// ACF flexible content
			if(  have_rows('components', $id) ):
				while ( have_rows('components', $id) ) : the_row();

						get_template_part( 'layouts/components/flex', get_row_layout() );

				endwhile; // close the loop of flexible content
			endif; // close flexible content conditional
			?>

		<?php endwhile; // end of the loop. ?>

		<?php
		/**
		 * generate_after_main_content hook.
		 *
		 * @since 0.1
		 */
		do_action( 'generate_after_main_content' );
		?>
		</div><!-- /wrapper -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php
/**
 * generate_after_primary_content_area hook.
 *
 * @since 2.0
 */
do_action( 'generate_after_primary_content_area' );

generate_construct_sidebars();


get_footer();
