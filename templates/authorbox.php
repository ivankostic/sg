<?php
$name = get_the_author_meta( 'display_name' );
$description = get_the_author_meta( 'description' );
$url = get_author_posts_url( get_the_author_meta( 'ID' ) );
$custom_avatar = get_field( 'user_avatar', 'user_' . get_the_author_meta( 'ID' ) );
$gravatar = get_author_posts_url( get_the_author_meta( 'ID' ) );

$image = null;

// If custom avatar isn't uploaded, try to grab from Gravatar
if ( $custom_avatar ) {
  $image = $custom_avatar;
} elseif ( !$custom_avatar && $gravatar ) {
  $image = get_avatar_url( get_the_author_meta( 'ID' ), ['size' => '80'] );
}
?>
<?php if ( get_the_author_meta( 'ID' ) ) : ?>
<div class="authorbox">
  <?php if ( $image ) : ?>
    <a class="authorbox__image-link" href="<?php echo $url ?>">
      <img class="authorbox__image"
      src="<?php echo $image ?>"
      alt="<?php echo $name ?>" />
    </a>
  <?php endif; ?>
  <h4 class="authorbox__name">
    <a class="authorbox__name-link" href="<?php echo $url ?>">
      <?php echo $name ?>
    </a>
  </h4>
  <?php echo $description ?>
</div>
<?php endif; ?>
