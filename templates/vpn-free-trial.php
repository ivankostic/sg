<?php
/*
 * Template Name: VPN Free Trial
 * Template Post Type: post, page
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

	<div id="primary" <?php generate_do_element_classes( 'content' ); ?>>
		<main id="main" <?php generate_do_element_classes( 'main', 'vpn-free-trial' ); ?>>
			<?php
			/**
			 * generate_before_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_before_main_content' );

			while ( have_posts() ) : the_post();

				get_template_part( 'content', 'page' ); ?>

<div class="vpn-free-trial-table">
<?php if( have_rows('vpns') ): $i = 0; ?>
  <?php while( have_rows('vpns') ): the_row(); $i++;

      $vpn_title = get_sub_field('vpn_title') ? get_sub_field('vpn_title') : 'VPN title not defined';
          $vpn_logo = get_sub_field('vpn_logo');
      $vpn_table_pros = get_sub_field('vpn_table_pros') ? get_sub_field('vpn_table_pros') : "";
    $rating    = get_sub_field('rating') ? get_sub_field('rating') : 100;
    $stars     = get_sub_field('stars') ? get_sub_field('stars') : 100;
    $affiliate_link_mobile  = get_sub_field('affiliate_link_mobile') ? get_sub_field('affiliate_link_mobile') : '#link-not-defined' ;
    $sanitized_title = sanitize_title($vpn_title);
  ?>
  <div class="vpn-free-trial-table-row">
    <div class="vpn-free-trial-table-50">
      <?php if (  $vpn_logo ) : ?>
      <a class="vpn-free-trial-table-logo vpnt-<?php echo $sanitized_title; ?>-logo-mb" href="<?php echo $affiliate_link_mobile; ?>" target="_blank">
        <img src="<?php echo $vpn_logo; ?>" alt="<?php echo $vpn_title; ?>" />
      </a>
      <?php endif; ?>
      <div class="star-ratings star-ratings--vpn-provider">
        <div class="star-ratings-top" style="width:<?php echo $stars; ?>%" rel="width:<?php echo $stars; ?>%">
          <span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
        </div>
        <div class="star-ratings-bottom">
          <span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
        </div>
      </div><!-- .star-ratings -->
      <p class="vpn-free-trial-table-score"><span class="score-text">OUR SCORE</span><span class="score-value"><?php echo $rating / 10; ?></span></p>
    </div><!-- vpn-free-trial-table-50 -->
    <div class="vpn-free-trial-table-50">
      <?php if ( $i === 1 ) : ?>
        <span class="editors-choice">Editor's choice!</span>
      <?php endif; ?>
      <div class="vpn-free-trial-table-pros <?php echo $i === 1 ? 'vpn-free-trial-table-pros--featured' : '' ?>">
        <?php echo $vpn_table_pros; ?>
      </div>
    </div><!-- .vpn-free-trial-table-50 -->
    <div class="vpn-free-trial-table-100">
      <a
         class="vpn-free-trial-table-btn <?php echo $i === 1 ? 'vpn-free-trial-table-btn--featured' : '';?> vpnt-<?php echo $sanitized_title; ?>-btn-mb"
         href="<?php echo $affiliate_link_mobile; ?>"
         target="_blank">Try <?php echo $vpn_title; ?> Free Trial <span>&rsaquo;</span>
      </a>
    </div><!-- .vpn-free-trial-table-100 -->
  </div><!-- .vpn-free-trial-table-row -->
  <?php endwhile; ?>
<?php endif; ?>
</div><!-- .vpn-free-trial-table -->

<?php if( have_rows('vpns') ): $i = 0; ?>
  <?php while( have_rows('vpns') ): the_row();

    // vars
    $vpn_title = get_sub_field('vpn_title') ? get_sub_field('vpn_title') : 'VPN title not defined';
    $rating    = get_sub_field('rating') ? get_sub_field('rating') : 100;
    $stars     = get_sub_field('stars') ? get_sub_field('stars') : 100;
    $affiliate_link_desktop = get_sub_field('affiliate_link_desktop') ? get_sub_field('affiliate_link_desktop') : '#link-not-defined' ;
    $affiliate_link_mobile  = get_sub_field('affiliate_link_mobile') ? get_sub_field('affiliate_link_mobile') : '#link-not-defined' ;
    $vpn_screenshot_desktop = get_sub_field('vpn_screenshot_desktop');
    $vpn_screenshot_mobile  = get_sub_field('vpn_screenshot_mobile');
    $vpn_intro = get_sub_field('vpn_intro') ? get_sub_field('vpn_intro') : 'VPN Intro not defined';
    $pros  	   = get_sub_field('pros') ? get_sub_field('pros') : 'VPN Pros not defined';
    $cons      = get_sub_field('cons') ? get_sub_field('cons') : 'VPN Cons not defined';
    $vpn_content = get_sub_field('vpn_content') ? get_sub_field('vpn_content') : 'VPN Content not defined';
    $vpn_footer  = get_sub_field('vpn_footer') ? get_sub_field('vpn_footer') : 'VPN Footer content not defined';
    $cta_button_label  = get_sub_field('cta_button_label') ? get_sub_field('cta_button_label') : 'Free Trial';
    $sanitized_title = sanitize_title($vpn_title);

$i++;
?>
<div class="vpn-provider <?php echo $i === 1 ? 'vpn-provider--featured' : '';?>">
  <div class="vpn-provider-header">
    <div class="content-container">
      <h2 class="vpn-provider-heading">
        <?php echo $i; ?>.
        <a class="hide-on-mobile vpnt-<?php echo $sanitized_title; ?>-title-dt" href="<?php echo $affiliate_link_desktop; ?>" target="_blank"><?php echo $vpn_title; ?></a>
        <a class="hide-on-desktop vpnt-<?php echo $sanitized_title; ?>-title-mb" href="<?php echo $affiliate_link_mobile; ?>" target="_blank"><?php echo $vpn_title; ?></a>
      </h2>
      <div class="star-ratings star-ratings--vpn-provider">
        <div class="star-ratings-top" style="width:<?php echo $stars; ?>%" rel="width:<?php echo $stars; ?>%">
          <span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
        </div>
        <div class="star-ratings-bottom">
          <span>★</span><span>★</span><span>★</span><span>★</span><span>★</span>
        </div>
      </div><!-- .star-ratings -->
      <a class="free-trial-btn top-right-abs hide-on-mobile vpnt-<?php echo $sanitized_title; ?>-header-btn-dt" href="<?php echo $affiliate_link_desktop; ?>" target="_blank"><?php echo $cta_button_label; ?></a>

      <?php if ( $i === 1 ) : // Only applies for featured item, other items have it displyed below (other place) ?>
        <a class="free-trial-btn free-trial-btn--header-featured top-right-abs hide-on-desktop vpnt-<?php echo $sanitized_title; ?>-header-btn-mb" href="<?php echo $affiliate_link_mobile; ?>" target="_blank"><?php echo $cta_button_label; ?></a>
      <?php endif; ?>

      <p><strong><?php echo $vpn_intro; ?></strong></p>
    </div>
    <?php if ( $vpn_screenshot_desktop && $vpn_screenshot_mobile ) : ?>
    <a href="<?php echo $affiliate_link_desktop; ?>" target="_blank">
      <img class="vpn-provider-screenshot hide-on-mobile" src="<?php echo $vpn_screenshot_desktop; ?>" alt="<?php echo $vpn_title; ?>" />
    </a>
    <a href="<?php echo $affiliate_link_mobile; ?>" target="_blank">
      <img class="vpn-provider-screenshot hide-on-desktop" src="<?php echo $vpn_screenshot_mobile; ?>" alt="<?php echo $vpn_title; ?>" />
    </a>
    <?php else: ?>
    <p>VPN's Desktop or Mobile screenshot image is missing.</p>
    <?php endif; ?>
  </div><!-- .vpn-provider-header -->

  <?php if ( $i > 1 ) : // Different layout for non-featured items ?>
  <div class="free-trial-btn-container hide-on-desktop-flex">
    <a class="free-trial-btn free-trial-btn--centered vpnt-<?php echo $sanitized_title; ?>-header-btn-mb" href="<?php echo $affiliate_link_mobile; ?>" target="_blank"><?php echo $vpn_title; ?> Free Trial</a>
  </div>
  <?php endif; ?>

  <div class="content-container">
    <div class="pros-cons-box">
      <div class="pros-box">
        <h3>What we like about <?php echo $vpn_title; ?></h3>
        <?php echo $pros; ?>
      </div>
      <div class="cons-box">
        <h3>What we don’t like</h3>
        <?php echo $cons; ?>
      </div>
    </div><!-- .pros-cons-box -->
    <div class="vpn-provider-content">
      <?php echo $vpn_content; ?>
    </div><!-- .vpn-provider-content -->
  </div><!-- .content-container -->
  <div class="vpn-provider-footer <?php echo $i === 1 ? 'vpn-provider-footer--featured' : '';?>">
    <div class="content-container">
      <h3>What you need to know about the
        <a class="hide-on-mobile vpnt-<?php echo $sanitized_title; ?>-wyntk-link-dt" href="<?php echo $affiliate_link_desktop; ?>"><?php echo $vpn_title; ?> Free Trial</a>
        <a class="hide-on-desktop vpnt-<?php echo $sanitized_title; ?>-wyntk-link-mb" href="<?php echo $affiliate_link_mobile; ?>"><?php echo $vpn_title; ?> Free Trial</a></h3>
      <?php echo $vpn_footer; ?>
    </div><!-- .content-container -->
  </div><!-- .vpn-provider-footer -->
  <div class="vpn-provider-cta <?php echo $i === 1 ? 'vpn-provider-cta--featured' : '';?>">
    <h3 class="vpn-provider-cta-title">
      <a class="hide-on-mobile vpnt-<?php echo $sanitized_title; ?>-cta-click-dt" href="<?php echo $affiliate_link_desktop; ?>">Click here</a>
      <a class="hide-on-desktop vpnt-<?php echo $sanitized_title; ?>-cta-click-mb" href="<?php echo $affiliate_link_mobile; ?>">Click here</a>
      to start your <?php echo $vpn_title; ?></h3>
    <a class="free-trial-btn <?php echo $i === 1 ? 'free-trial-btn--green' : '';?> hide-on-mobile-flex vpnt-<?php echo $sanitized_title; ?>-cta-btn-dt" href="<?php echo $affiliate_link_desktop; ?>" target="_blank"><?php echo $cta_button_label; ?></a>
    <a class="free-trial-btn <?php echo $i === 1 ? 'free-trial-btn--green' : '';?> hide-on-desktop-flex vpnt-<?php echo $sanitized_title; ?>-cta-btn-mb" href="<?php echo $affiliate_link_mobile; ?>" target="_blank"><?php echo $cta_button_label; ?></a>
  </div><!-- .vpn-provider-cta -->
</div><!-- .vpn-provider -->

  <?php endwhile; ?>
<?php endif; ?>

<div class="vpn-free-trial-footer">
  <div class="content-container">
    <?php
    if (get_field('authors_notes')) :
      echo get_field('authors_notes');
    endif;
    ?>
  </div>
</div><!-- .vpn-free-trial-footer -->
        <?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || '0' != get_comments_number() ) : ?>

					<div class="comments-area">
						<?php comments_template(); ?>
					</div>

				<?php endif;

			endwhile;

			/**
			 * generate_after_main_content hook.
			 *
			 * @since 0.1
			 */
			do_action( 'generate_after_main_content' );
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

	<?php
	/**
	 * generate_after_primary_content_area hook.
	 *
	 * @since 2.0
	 */
	do_action( 'generate_after_primary_content_area' );

	generate_construct_sidebars();

get_footer();
